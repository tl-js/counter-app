import React, { Component } from "react";

class Counter extends Component {
  render() {
    console.log("Counter - Rendered");
    return (
      <div>
        <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
        <button
          onClick={() => this.props.onIncrement(this.props.counter)}
          className="btn btn-secondary btn-sm"
        >
          Increment
        </button>
        <button
          onClick={() => this.props.onDelete(this.props.counter.id)}
          className="btn btn-danger btn-sm m-2"
        >
          Delete
        </button>
      </div>
    );
  }

  componentDidUpdate(prevProps, prevState) {
    console.log("Counter - Updated", prevProps, prevState);
    // decide if we need to make ajax call here
    if (prevProps.counter.value !== this.props.counter.value) {
      console.log("diff");
    }
  }

  componentWillUnmount() {
    console.log("Counter - Unmount");
  }

  getBadgeClasses() {
    let classes = "badge m-2 badge-";
    classes += this.props.counter.value === 0 ? "warning" : "primary";
    return classes;
  }

  formatCount() {
    const { value } = this.props.counter;
    return value === 0 ? "Zero" : value;
  }
}

export default Counter;
